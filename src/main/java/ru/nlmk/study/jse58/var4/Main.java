package ru.nlmk.study.jse58.var4;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import ru.nlmk.study.jse58.var4.config.HibernateConfig;
import ru.nlmk.study.jse58.var4.model.BankAccount;
import ru.nlmk.study.jse58.var4.model.CreditCard;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        CreditCard creditCard = new CreditCard();
        creditCard.setCardNumber("34343444434");
        creditCard.setExpMonth("May");
        creditCard.setExpYear("2024");
        creditCard.setOwner("Mikhail Ivanov");

        BankAccount bankAccount = new BankAccount();
        bankAccount.setBankName("Sber");
        bankAccount.setOwner("Andrey Petrov");
        bankAccount.setSwift("dfdf");
        bankAccount.setAccount("994823498734587");

        SessionFactory sessionFactory = HibernateConfig.getSessionFactory();

        Session session;
        Transaction transaction = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            session.persist(creditCard);
            session.persist(bankAccount);
            transaction.commit();
        } catch (Exception e){
            transaction.rollback();
            throw e;
        }

        try{
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            List billingDetails = session.createQuery("select bd from BillingDetails bd").list();
            for (int i = 0; i < billingDetails.size(); i++){
                System.out.println(billingDetails.get(i));
            }
        } catch (Exception e){{
            transaction.rollback();
            throw e;
        }}
    }
}
