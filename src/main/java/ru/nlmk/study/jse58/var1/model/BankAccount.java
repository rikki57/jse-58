package ru.nlmk.study.jse58.var1.model;

import javax.persistence.*;

@Entity
@Table(name = "BANK_ACCOUNT")
public class BankAccount extends BillingDetails {
    public BankAccount() {
    }

    private String swift;

    @Column(name = "bank_name")
    private String bankName;

    private String account;

    public String getSwift() {
        return swift;
    }

    public void setSwift(String swift) {
        this.swift = swift;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    @Override
    public String toString() {
        return "BankAccount{" +
                ", swift='" + swift + '\'' +
                ", bankName='" + bankName + '\'' +
                '}';
    }
}
